/**
 * Created by SE on 15/11/2015.
 */
angular.module('hotelApp')
    .service('reserveService', function ($q) {
        var self = this;
        self.reserve = function (hotel, room) {
            var deferred = $q.defer();
            $.getJSON("app/data/data.json", function (data) {
                var hotelInfo = [];
                var roomInfo = [];
                /*hotel = 1;
                room = 1;*/
                /**
                 * checking if hotel is greater than 0 and not a string
                 */
                if (hotel > 0 && !isNaN(hotel)) {
                    hotelInfo = data.filter(function (obj) {
                        return obj.id == hotel;
                    });
                    /**
                     * same with room
                     */
                    if (room > 0 && !isNaN(room)) {
                        roomInfo = hotelInfo[0].room.filter(function (obj) {
                            return obj.id == room;
                        });
                    }
                }
                var result;
                /**
                 * setting hotel values if it exists
                 */
                if (hotelInfo.length != 0) {
                    result = {
                        hotel: hotelInfo[0].name,
                        location: hotelInfo[0].location,
                        hotelImg: hotelInfo[0].mainImg,
                        hotelRanking: hotelInfo[0].ranking,
                        hotelRooms:hotelInfo[0].room
                    };
                    /**
                     * same with room
                     */
                    if (roomInfo.length != 0) {
                        result.roomName = roomInfo[0].name;
                        result.roomPrice = roomInfo[0].price;
                        result.roomDesc = roomInfo[0].description;
                        result.roomImg = roomInfo[0].gallery[0];
                        result.days = roomInfo[0].date;
                    }

                }else{
                    result = {
                        hotel: 0,
                        location: 0,
                        hotelImg: 0,
                        hotelRanking:0,
                        hotelRooms:0,
                        roomName : 0,
                        roomPrice : 0,
                        roomDesc : 0,
                        roomImg : 0,
                        days :0
                    }
                }
                result.data = data;
                /**
                 * if result is undefined i'll handle it in controller
                 */
                deferred.resolve(result);
            });
            return deferred.promise;
        };
        self.getinit = function(){
            var deferred = $q.defer();
            $.getJSON("app/data/data.json", function (data) {
                deferred.resolve(data[0]);
            });
            return deferred.promise;
        }
    });