/**
 * Created by JUANPABLO on 7/11/2015.
 */
angular.module('hotelApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('reserve', {
                url: '/reserve',
                templateUrl: 'app/reserve/reserve.html',
                controller: 'reserveController',
                controllerAs: 'reserveController'
            })
    });