/**
 * Created by SE on 15/11/2015.
 */
angular.module('hotelApp')
    .controller('reserveController', function (reserveService, reserveFactory) {
        var self = this;
        self.reserve = [];
        self.totalPrice = 0;
        self.dayPrice = 0;
        reserveService.reserve(reserveFactory.data.hotelId, reserveFactory.data.roomId).then(function (result) {
            self.reserve = result;
            self.currentHotel = self.reserve.data.filter(function (obj) {
                return obj.name == self.reserve.hotel;
            });
            self.totalPrice = self.reserve.roomPrice;
            self.dayPrice = Number(self.totalPrice);
        });
        reserveService.getinit().then(function (result) {
            self.init = result;
        });
        self.meal = {};
        self.totalDays = 1;
        self.food = function () {
            self.dayPrice = (Number(self.reserve.roomPrice) + Number(self.meal.value));
            self.setTotal();
        };
        self.setTotal = function () {

            self.totalPrice = (Number(self.dayPrice) * Number(self.totalDays));

        };
        self.meals = [{name: "Desayuno", value: "10000"}, {
            name: "Desayuno y Almuerzo",
            value: "20000"
        }, {name: "Todas las comidas", value: "30000"}];
        self.date = new Date();
        self.minDate = new Date();
        self.maxDate = new Date();
        self.newMinDate = function () {
            if (self.date > self.maxDate) {
                self.maxDate = self.date;
            }

            if (Math.ceil(Math.abs(self.date.getTime() - self.maxDate.getTime()) / (1000 * 3600 * 24)) >= 1) {
                self.totalDays = Math.ceil(Math.abs(self.date.getTime() - self.maxDate.getTime()) / (1000 * 3600 * 24));

                self.setTotal();
            } else {
                self.totalDays = 1;
                self.setTotal();
            }
        };
        self.random = function(){
            return Math.random()*Math.pow(2, 53);
        };
        /*self.getinit = function () {
         console.log("entro");
         return self.reserve.data.filter(function (obj) {
         return obj.name == self.reserve.hotel;
         });
         };*/
        self.hotelChange = function () {
            self.reserve.hotelRooms = self.currentHotel.room;
            self.reserve.hotelImg = self.currentHotel.mainImg;
            /*self.reserve.hotelRooms = self.reserve.filter(function (obj){
             return obj.id == hotel;
             });*/
        }
    });