/**
 * Created by JUANPABLO on 7/11/2015.
 */

var hotelApp = angular.module('hotelApp', [
    'ui.router',
    'angularMoment',
    'angular.chosen'
])
    .config(function ($urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);
    });


