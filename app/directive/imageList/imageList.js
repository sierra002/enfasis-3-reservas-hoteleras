/**
 * Created by SE on 15/11/2015.
 */
angular.module('hotelApp')
    .directive('imgdirective', function () {
        return {
            templateUrl: 'app/directive/imageList/imageList.template.html',
            restrict: 'EA',//defined as element or attribute so it can be used by bot ways
            scope: {
                // name : '@' only if your attr came using {{controller.attr}}, else use '=' when you get by homeController.countryName
                name: '=',
                image:'='
            },
            link: function (scope, element, attr) {

            }
        };
    });