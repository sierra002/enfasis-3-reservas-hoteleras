/**
 * Created by SE on 16/11/2015.
 */
angular.module('hotelApp')
    .directive('rating', function () {
        return {
            templateUrl: 'app/directive/rating/rating.template.html',
            restrict: 'EA',//defined as element or attribute so it can be used by bot ways
            scope: {
                // name : '@' only if your attr came using {{controller.attr}}, else use '=' when you get by homeController.countryName
                star: '='
            },
            link: function (scope, element, attr) {
                var maxStars = 5;
                if (!isNaN(scope.star)) {
                    if (scope.star <= maxStars) {
                        scope.rating = new Array(scope.star);
                        scope.norating = new Array(maxStars - scope.star);
                    }else{
                        scope.rating = new Array(maxStars);
                        scope.norating = new Array(0);
                    }
                }
            }
        };
    });