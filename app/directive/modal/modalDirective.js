/**
 * Created by seryo on 16/11/2015.
 */
angular.module('hotelApp')
    .directive('modalDirective', function () {
        return {
            templateUrl: 'app/directive/modal/modalDirective.template.html',
            restrict: 'EA',
            scope:{
                title: '=modalTitle',
                header: '=modalHeader',
                body: '=modalBody',
                footer: '=modalFooter',
                handler: '=lolo'
            },
            link: function(scope, element, attr){
                console.log('directive', scope.show);
            },
            controller: function ($scope) {
                $scope.handler = 'pop';
            }
        };
    });