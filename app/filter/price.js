/**
 * Created by JUANPABLO on 17/11/2015.
 */

angular.module('hotelApp')
    .filter('price', function(){
        return function(number) {
            if (isNaN(number) || number <= 0) {
                return '-';
            }
            else {
                if (number <= 150000) {
                    return '$';
                }
                else if (150000 < number && number < 300000) {
                    return '$$';
                }
                else {
                    return '$$$';
                }
            }
        }
    });

