/**
 * Created by SE on 11/7/15.
 */
angular.module('hotelApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('who-are-we', {
                url: '/who-are-we',
                templateUrl: 'app/whoAreWe/waw.html',
                controller: 'wawController',
                controllerAs: 'wawController'
            })
    });