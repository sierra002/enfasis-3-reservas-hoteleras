/**
 * Created by SE on 11/7/15.
 */
angular.module('hotelApp')
    .controller('wawController', function (wawService) {
        var self = this;
        wawService.data().then(function (result) {
            self.waw = result.whoAreWe;
        });
    });