/**
 * Created by seryo on 7/11/2015.
 */
angular.module('hotelApp')
.config(function($stateProvider){
       $stateProvider
           .state('hotelList', {
               url: '/hotelList',
               templateUrl: 'app/hotelList/hotelList.html',
               controller: 'hotelListController',
               controllerAs: 'hotelListController'
           })
    });