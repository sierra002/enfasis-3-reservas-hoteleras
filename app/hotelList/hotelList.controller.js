/**
 * Created by seryo on 7/11/2015.
 */
angular.module('hotelApp')
.controller('hotelListController', function(hotelListService, reserveFactory, $state){
        var self = this;
        self.state = true;

        self.hotelList = [];
        var promise = hotelListService.hotelList();

        promise.then(function(response){
            self.hotelList = response.data;
        });

        self.idHotel = function(id){
            reserveFactory.data.hotelId = id;
            var select = $('#selectChosen');
            if (select.val() != '?' && self.state){
                $state.go('hotel');
                self.state = false;
            }else if(!self.state){
                $state.reload(true);
            }
        };

    });