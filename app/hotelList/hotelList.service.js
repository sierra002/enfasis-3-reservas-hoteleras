/**
 * Created by seryo on 7/11/2015.
 */
angular.module('hotelApp')
.service('hotelListService',function($http){
        var self = this;
        self.hotelList = function(){
            return $http({
                method: 'GET',
                url: 'app/data/data.json'
            })
        }
    });