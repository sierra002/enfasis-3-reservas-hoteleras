/**
 * Created by SE on 15/11/2015.
 */
angular.module('hotelApp')
    .factory("reserveFactory", function () {
        /*
         -hotelId para los ids de los hoteles de aca se coge cuando
         se llega a la pesta�a de hotel el id que se necesita.
         -roomId para los cuartos, necesita el id del hotel para mostrar toda la lista de los cuartos.
         al final si pasa por el roomId me tienen que mandar ambos valores a la pesta�a de las reservas,
         por que si no, pongo a llenar lo que quede faltando
         si no me llega el hotel si pongo a llenar todo pq graves adivinarlo*/
        return {
            data: {"hotelId": 0, "roomId": 0}
        }
    });