/**
 * Created by JUANPABLO on 7/11/2015.
 */

angular.module('hotelApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('hotel', {
                url: '/hotel',
                templateUrl: 'app/hotel/hotel.html',
                controller: 'hotelController',
                controllerAs: 'hotelController'
            })
    });