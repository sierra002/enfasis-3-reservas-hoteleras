/**
 * Created by JUANPABLO on 7/11/2015.
 */

angular.module('hotelApp')
    .service('hotelService', function($q,$http){
        var self = this;
        var deferred = $q.defer();

        self.hotels = function(){
            $http.get('app/data/data.json')
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (err) {
                    console.log('error', err);
                });
            return deferred.promise;
        };

    });


