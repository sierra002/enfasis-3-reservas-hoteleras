/**
 * Created by JUANPABLO on 7/11/2015.
 */
angular.module('hotelApp')
    .service('roomService', function ($http, $q) {
        var self = this;
        var deferred = $q.defer();

        self.rooms = function(){
            $http.get('app/data/data.json')
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (err) {
                    console.log('error', err);
                });
            return deferred.promise;
        };

    });