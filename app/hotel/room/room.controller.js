/**
 * Created by JUANPABLO on 7/11/2015.
 */
angular.module('hotelApp')
    .controller('roomController', function (roomService, reserveFactory) {
        var self = this;
        var promise = roomService.rooms();

        promise.then(function(response){
            var result = response.filter(function(obj){
                return obj.id == reserveFactory.data.hotelId;
            });
            self.rooms = result[0].room;
        });

        self.toggleModal = function(room) {
            self.roomCarousel = room;
            $('#modalRoom').modal('toggle');
        };

        self.idRoom = function(id){
            reserveFactory.data.roomId = id;
        };

    });