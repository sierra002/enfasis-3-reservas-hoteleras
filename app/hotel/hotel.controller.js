/**
 * Created by JUANPABLO on 7/11/2015.
 */

angular.module('hotelApp')
    .controller('hotelController', function (hotelService,reserveFactory) {
        var self = this;
        var promise = hotelService.hotels();

        promise.then(function(response){
            var result = response.filter(function(obj){
                return obj.id == reserveFactory.data.hotelId;
            });
            self.hotels = result;
        });

        //self.toggleModal = function(hotel) {
        //    self.roomCarousel = hotel;
        //    $('#modalRoom').modal('toggle');
        //};

        self.idHotel = function(id){
            reserveFactory.data.hotelId = id;
        };

    });

