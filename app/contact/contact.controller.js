/**
 * Created by SE on 11/7/15.
 */
angular.module('hotelApp')
    .controller('contactController', function (contactService) {
        var self = this;
        contactService.data().then(function (result) {
            var data = result.contact;
            self.contact = data.FAQ;
            self.calls = data.calls;
        });
    });