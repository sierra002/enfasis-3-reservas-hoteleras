/**
 * Created by SE on 11/7/15.
 */
angular.module('hotelApp')
    .service('contactService', function($q){
        var self = this;
        self.data = function(){
            var deferred = $q.defer();
            $.getJSON( "app/data/info.json", function( data ) {
                deferred.resolve(data);
            });
            return deferred.promise;
        };
    });



