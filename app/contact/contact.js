/**
 * Created by SE on 11/7/15.
 */
angular.module('hotelApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('contact', {
                url: '/contact',
                templateUrl: 'app/contact/contact.html',
                controller: 'contactController',
                controllerAs: 'contactController'
            })
    });