/**
 * Created by SE on 11/7/15.
 */
angular.module('hotelApp')
    .service('homeService', function ($q) {
        var self = this;
        self.data = function () {
            var deferred = $q.defer();
            $.getJSON("app/data/data.json", function (data) {
                deferred.resolve(data);
            });
            return deferred.promise;
        };
        self.array = function () {
            var deferred = $q.defer();
            $.getJSON("app/data/data.json", function (data) {
                var all_hotel_img = [];
                $.each(data,function(index,data1){
                    for(i=0;i<data1.gallery.length;i++){
                        all_hotel_img.push(data1.gallery[i]);
                    }
                });
                deferred.resolve(all_hotel_img);
            });
            return deferred.promise;
        };
    });



