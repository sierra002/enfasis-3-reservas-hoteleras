/**
 * Created by SE on 11/7/15.
 */
angular.module('hotelApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/home/home.html',
                controller: 'homeController',
                controllerAs: 'homeController'
            })
    });