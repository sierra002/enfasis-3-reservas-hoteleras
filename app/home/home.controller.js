/**
 * Created by SE on 11/7/15.
 */
angular.module('hotelApp')
    .controller('homeController', function (homeService, reserveFactory, $state) {
        var self = this;
        homeService.data().then(function (result) {
            result.sort(function(a, b) {return a['ranking'] - b['ranking']});
            self.data = [];
            for(var i = result.length; i>result.length-3;i--){
                self.data.push(result[i-1]);
            }
            /*self.data = result;*/
        });
        homeService.array().then(function (result) {
            var length =result.length;
            var random1 = Math.random()*length;
            var random2;
            if(random1>(length/2)){
                random2 = Math.random()*(length/2);
            }else{
                random2 = Math.random()*(length/2) + length/2;
            }
            if(random1>random2){
                result = result.splice(parseInt(random2),parseInt(random1));
            }else{
                result = result.splice(parseInt(random1),parseInt(random2));
            }
            self.array = result;
        });
        self.goToHotel = function (hotelId) {
            reserveFactory.data.hotelId = hotelId;
            $state.go('hotel');
        }
    });